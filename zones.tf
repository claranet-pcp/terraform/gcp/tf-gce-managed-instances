data "google_compute_zones" "zones" {
  region = "${var.gcp_region}"
  status = "UP"
}
