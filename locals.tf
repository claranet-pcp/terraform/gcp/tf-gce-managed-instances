locals {
  is_persistent  = "${element(split("_", var.template_type), 1) == "persistent" ? true : false}"
  is_static      = "${var.template_type == "static" ? true : local.is_persistent}"
  zonal_group    = "${element(split("_", var.template_type), 0) == "static" ? true : local.is_persistent}"
  regional_group = "${local.zonal_group ? false : true}"

  default_metadata = {
    envname                    = "${var.envname}"
    profile                    = "${var.service}"
    domain                     = "${var.domain}"
    startup-script-url         = "${var.os == "linux" ? var.startup_script : "" }"
    windows-startup-script-ps1 = "${var.os == "windows" ? var.startup_script : "" }"
  }
}
