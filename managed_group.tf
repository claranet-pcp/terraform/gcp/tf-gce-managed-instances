resource "google_compute_region_instance_group_manager" "managed_group" {
  count = "${!var.autoscaler && local.regional_group ? 1 : 0}"

  name               = "${var.service}-${var.gcp_region}"
  base_instance_name = "${var.service}"
  instance_template  = "${element(compact(concat("${google_compute_instance_template.instance_template-private.*.self_link}","${google_compute_instance_template.instance_template-private_persistent.*.self_link}","${google_compute_instance_template.instance_template-public.*.self_link}","${google_compute_instance_template.instance_template-public_persistent.*.self_link}","${google_compute_instance_template.instance_template-static.*.self_link}","${google_compute_instance_template.instance_template-static_persistent.*.self_link}")), count.index)}"
  region             = "${var.gcp_region}"

  named_port {
    name = "${var.named_port["name"]}"
    port = "${var.named_port["port"]}"
  }

  target_size  = "${var.number_of_zones}"
  target_pools = ["${var.target_pools}"]

  depends_on = [
    "google_compute_instance_template.instance_template-private",
    "google_compute_instance_template.instance_template-public",
    "google_compute_instance_template.instance_template-static",
    "google_compute_instance_template.instance_template-private_persistent",
    "google_compute_instance_template.instance_template-public_persistent",
    "google_compute_instance_template.instance_template-static_persistent",
  ]
}

resource "google_compute_region_instance_group_manager" "autoscaled_managed_group" {
  count = "${var.autoscaler ? 1 : 0}"

  name               = "${var.service}-${var.gcp_region}"
  base_instance_name = "${var.service}"
  instance_template  = "${element(compact(concat("${google_compute_instance_template.instance_template-private.*.self_link}","${google_compute_instance_template.instance_template-private_persistent.*.self_link}","${google_compute_instance_template.instance_template-public.*.self_link}","${google_compute_instance_template.instance_template-public_persistent.*.self_link}","${google_compute_instance_template.instance_template-static.*.self_link}","${google_compute_instance_template.instance_template-static_persistent.*.self_link}")), count.index)}"
  region             = "${var.gcp_region}"

  named_port {
    name = "${var.named_port["name"]}"
    port = "${var.named_port["port"]}"
  }

  target_pools = ["${var.target_pools}"]

  depends_on = [
    "google_compute_instance_template.instance_template-private",
    "google_compute_instance_template.instance_template-public",
    "google_compute_instance_template.instance_template-static",
    "google_compute_instance_template.instance_template-private_persistent",
    "google_compute_instance_template.instance_template-public_persistent",
    "google_compute_instance_template.instance_template-static_persistent",
  ]
}

resource "google_compute_instance_group_manager" "managed_group" {
  count = "${local.zonal_group ? var.number_of_zones : 0}"

  name               = "${var.service}-${element(data.google_compute_zones.zones.names, count.index)}"
  base_instance_name = "${var.service}-${count.index}"
  instance_template  = "${element(compact(concat("${google_compute_instance_template.instance_template-private.*.self_link}","${google_compute_instance_template.instance_template-private_persistent.*.self_link}","${google_compute_instance_template.instance_template-public.*.self_link}","${google_compute_instance_template.instance_template-public_persistent.*.self_link}","${google_compute_instance_template.instance_template-static.*.self_link}","${google_compute_instance_template.instance_template-static_persistent.*.self_link}")), count.index)}"
  update_strategy    = "${var.update_strategy}"
  zone               = "${element(data.google_compute_zones.zones.names, count.index)}"

  named_port {
    name = "${var.named_port["name"]}"
    port = "${var.named_port["port"]}"
  }

  target_size  = 1
  target_pools = ["${var.target_pools}"]

  depends_on = [
    "google_compute_instance_template.instance_template-private",
    "google_compute_instance_template.instance_template-public",
    "google_compute_instance_template.instance_template-static",
    "google_compute_instance_template.instance_template-private_persistent",
    "google_compute_instance_template.instance_template-public_persistent",
    "google_compute_instance_template.instance_template-static_persistent",
  ]
}
