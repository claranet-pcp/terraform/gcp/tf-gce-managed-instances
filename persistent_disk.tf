resource "google_compute_disk" "persistent_disk" {
  count = "${element(split("_", var.template_type), 1) == "persistent" ? var.number_of_zones : 0}"

  lifecycle {
    create_before_destroy = true
  }

  name = "${var.service}-${element(data.google_compute_zones.zones.names, count.index)}"
  type = "${var.persistent_disk_type}"
  zone = "${element(data.google_compute_zones.zones.names, count.index)}"
  size = "${var.persistent_disk_size}"
}
