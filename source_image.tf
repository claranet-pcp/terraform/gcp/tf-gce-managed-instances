data "google_compute_image" "source_image" {
  family  = "${var.disk_image}"
  project = "${var.project}"
}
