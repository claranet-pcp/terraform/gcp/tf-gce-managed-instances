To launch test:

```
make
make apply
```

To destroy created infrastructure:

```
make destroy
```

This test requires a network/subnetwork to be available to launch instances into, hence https://git.bashton.net/Bashton-Terraform-Modules/tf-gcp-standardsubnetwork is required.

This test makes use of Regional Managed Instance Groups to create fixed size and autoscaling groups of instances across a region's zones.

It is assumed that instance groups requiring persistent disks are fixed in size (number of nodes) and spread equaly across a region's zones (max 3 == 3 zones). For this individual managed groups (at the zone layer) are required to incorporate a working template.

#### Use Cases

| Type of Managed Group        | Private | Ephemeral | Static |
| -----------------------------|:--------:|:--------:|:------:|
| Autoscaling                  | Regional | Regional | na     |
| Fixed Size                   | Regional | Regional | Zonal  |
| Fixed Size w/Persistent Disk | Zonal    | Zonal    | Zonal  |

