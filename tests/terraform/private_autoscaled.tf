module "private-a-instance-template" {
  source = "../.."

  template_type     = "private"
  machine_type      = "${var.machine_type}"
  preemptible       = "${var.preemptible}"
  automatic_restart = "${var.automatic_restart}"
  update_strategy   = "${var.update_strategy}"
  number_of_zones   = "${var.number_of_zones}"
  envname           = "${var.envname}"
  service           = "private-a"
  autoscaler        = "true"
  domain            = "${var.domain}"
  fw_tags           = ["private-a", "common"]
  needs_nat         = "natted"
  startup_script    = "gs://some_bucket/bootstrap/startup.sh"
  disk_image        = "${var.disk_image}"
  net_name          = "${module.subnet_euw1.name}"
  ip_forward        = "false"
  gcp_region        = "${var.gcp_region}"
  project           = "${var.gcp_project}"

  scopes = [
    "storage-ro",
    "compute-ro",
    "logging-write",
  ]
}
