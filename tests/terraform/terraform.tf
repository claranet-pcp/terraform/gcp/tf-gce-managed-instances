variable "account_file" {
  description = "GCP JSON account file"
  default     = "gce-account.json"
}

variable "gcp_project" {
  default = "intrepid-signal-149714"
}

variable "gcp_region" {
  default = "europe-west1"
}

provider "google" {
  credentials = "${file(var.account_file)}"
  project     = "${var.gcp_project}"
  region      = "${var.gcp_region}"
}

variable "envname" {
  default = "pietest"
}

variable "disk_image" {
  default = "pie-debian9"
}

variable "update_strategy" {
  default = "RESTART"
}

variable "domain" {
  default = "timeforadietcokebreak.com"
}

variable "preemptible" {
  default = "true"
}

variable "automatic_restart" {
  default = "false"
}

variable "machine_type" {
  default = "g1-small"
}

variable "asg_min" {
  default = 2
}

variable "asg_max" {
  default = 6
}

variable "number_of_zones" {
  default = 2
}
