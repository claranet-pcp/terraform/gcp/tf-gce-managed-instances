resource "google_compute_network" "testing" {
  name                    = "testing-${var.envname}"
  auto_create_subnetworks = false
}

module "subnet_euw1" {
  source      = "git::ssh://git@gogs.bashton.net/Bashton-Terraform-Modules/tf-gcp-standardsubnetwork.git"
  name        = "testing-${var.envname}"
  network     = "${google_compute_network.testing.name}"
  region      = "${var.gcp_region}"
  subnet_cidr = "10.100.0.0/21"
}
