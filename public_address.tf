resource "google_compute_address" "public_address" {
  count = "${element(split("_", var.template_type), 0) == "static" ? var.number_of_zones : 0}"

  name   = "${var.service}-address-${count.index}"
  region = "${var.gcp_region}"
}
