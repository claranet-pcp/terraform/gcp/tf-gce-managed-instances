# Instance template for managed instance groups - SUBNETS ENABLED
resource "google_compute_instance_template" "instance_template-static" {
  count = "${var.template_type == "static" ? 1 : 0}"

  lifecycle {
    create_before_destroy = true
    ignore_changes        = ["automatic_restart"]
  }

  name_prefix    = "${var.service}-template-"
  can_ip_forward = "${var.ip_forward}"
  machine_type   = "${var.machine_type}"
  region         = "${var.gcp_region}"

  tags = [
    "${var.envname}",
    "${var.service}",
    "${var.needs_nat}",
    "${var.fw_tags}",
  ]

  disk {
    device_name  = "${var.disk_device_name}"
    source_image = "${data.google_compute_image.source_image.self_link}"
    boot         = true
    disk_type    = "${var.boot_disk_type}"
    disk_size_gb = "${var.boot_disk_size}"
  }

  network_interface {
    subnetwork = "${var.net_name}"

    access_config {
      nat_ip = "${element(google_compute_address.public_address.*.address, count.index)}"
    }
  }

  metadata = "${merge(local.default_metadata, var.additional_metadata)}"

  service_account {
    scopes = ["${var.scopes}"]
  }

  scheduling {
    preemptible       = "${var.preemptible}"
    automatic_restart = "${var.automatic_restart}"
  }
}
