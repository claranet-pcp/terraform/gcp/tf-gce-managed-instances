# Instance template variables
variable "envname" {
  description = "The value for the 'envname' label in the instance metadata, will also be added to networking tags"
  type        = "string"
}

variable "service" {
  description = "The value for the 'service' label in the instance metadata, will also be added to networking tags"
  type        = "string"
}

variable "domain" {
  description = "The value for the 'domain' label in the instance metadata"
  type        = "string"
}

variable "additional_metadata" {
  description = "Additional custom metadata"
  type        = "map"
  default     = {}
}

variable "os" {
  description = "OS is linux OR windows"
  type        = "string"
  default     = "linux"
}

variable "startup_script" {
  description = "The content of the instance startup script"
  type        = "string"
}

variable "disk_image" {
  description = "The name of the OS image to use for instances"
  type        = "string"
}

variable "boot_disk_type" {
  description = "The type of disk you require for the boot disk, pd-standard or pd-ssd, defaults to pd-standard"
  type        = "string"
  default     = "pd-standard"
}

variable "boot_disk_size" {
  description = "The size of the boot disk in GB"
  type        = "string"
  default     = 0
}

variable "project" {
  description = "The project to pull the image from. eg. GCP Project ID for custom, windows-cloud for a Windows image, debian-cloud for a debian image etc"
  type        = "string"
}

variable "net_name" {
  description = "The name of the network to connect instances to"
  type        = "string"
}

variable "gcp_region" {
  description = "The GCP region in which to create instances"
  type        = "string"
}

variable "template_type" {
  description = "The template type to use to create the instance"
  type        = "string"
  default     = "public"
}

variable "fw_tags" {
  description = "List of additional networking tags to add to instances"
  type        = "list"
}

variable "needs_nat" {
  description = "Value determines whether the instance has access to NAT"
  type        = "string"
  default     = "nat"
}

variable "machine_type" {
  description = "The GCP machine type to use when creating instances"
  type        = "string"
  default     = "g1-small"
}

variable "disk_device_name" {
  description = "The device name for the OS disk"
  type        = "string"
  default     = "sda"
}

variable "persistent_disk" {
  description = "Must be specified if a persistent type is used"
  type        = "string"
  default     = "dummy_disk"
}

variable "persistent_disk_name" {
  description = "The name of the persistent disk to add"
  type        = "string"
  default     = "sdc"
}

variable "persistent_disk_size" {
  description = "The size of the persistent disk to add"
  type        = "string"
  default     = 50
}

variable "persistent_disk_type" {
  description = "The type of the persistent disk to add"
  type        = "string"
  default     = "pd-standard"
}

variable "number_of_zones" {
  description = "The number of zones to autoscale across"
  type        = "string"
  default     = 1
}

variable "target_pools" {
  description = "List of target pools to use"
  type        = "list"
  default     = []
}

variable "scopes" {
  description = "The security scopes to add to instances"
  type        = "list"

  default = [
    "storage-ro",
    "compute-ro",
  ]
}

# Needed for NAT
variable "ip_forward" {
  description = "Bool indicating whether to allow IP forwarding to instances"
  type        = "string"
  default     = true
}

variable "preemptible" {
  description = "Bool indicating whether instances should be preemptible"
  type        = "string"
  default     = false
}

variable "automatic_restart" {
  description = "Bool indicating whether instances should auto restart if required during maintenance windows"
  type        = "string"
  default     = "true"
}

variable "update_strategy" {
  description = "If the instance_template resource is modified, a value of 'NONE' will prevent any of the managed instances from being restarted by Terraform. A value of 'RESTART' will restart all of the instances at once"
  type        = "string"
  default     = "RESTART"
}

variable "autoscaler" {
  description = "Bool indicating whether to autoscale resources"
  type        = "string"
  default     = false
}

variable "asg_max" {
  description = "The maximum desired count of instances for the autoscale group to maintain"
  type        = "string"
  default     = 3
}

variable "asg_min" {
  description = "The minimum desired count of instances for the autoscale group to maintain"
  type        = "string"
  default     = 1
}

variable "cpu_utilization" {
  description = "The target CPU utilisation for an autoscale event to occur"
  type        = "string"
  default     = "0.6"
}

variable "named_port" {
  description = "Instance Group named port"
  type        = "map"

  default = {
    name = "http"
    port = "80"
  }
}
