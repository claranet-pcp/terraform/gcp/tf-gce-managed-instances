## 0.1.1 (Jan 26, 2018)

IMPROVEMENTS:

  * **Metadata** Allow for custom metadata in addition to a set of
    defaults

## 0.1.0 (Dec 15, 2017)

FEATURES:

  * core: support both `zonal` and `regional` instance group managers

IMPROVEMENTS:

  * **Parameterise named port**: add ability to update instance groups' named port

BUG FIXES:

  * core: fix updating instance template
