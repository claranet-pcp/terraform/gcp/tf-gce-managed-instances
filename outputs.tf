output "link-private" {
  value = ["${google_compute_instance_template.instance_template-private.*.self_link}"]
}

output "link-private_persistent" {
  value = ["${google_compute_instance_template.instance_template-private_persistent.*.self_link}"]
}

output "link-public" {
  value = ["${google_compute_instance_template.instance_template-public.*.self_link}"]
}

output "link-public_persistent" {
  value = ["${google_compute_instance_template.instance_template-public_persistent.*.self_link}"]
}

output "link-static" {
  value = ["${google_compute_instance_template.instance_template-static.*.self_link}"]
}

output "link-static_persistent" {
  value = ["${google_compute_instance_template.instance_template-static_persistent.*.self_link}"]
}

output "instance_group" {
  value = ["${google_compute_region_instance_group_manager.managed_group.*.instance_group}"]
}

output "autoscaled_instance_group" {
  value = ["${google_compute_region_instance_group_manager.autoscaled_managed_group.*.instance_group}"]
}

output "external_ip" {
  value = ["${google_compute_address.public_address.*.address}"]
}
