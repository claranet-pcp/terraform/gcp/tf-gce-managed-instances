resource "google_compute_region_autoscaler" "autoscaler" {
  count = "${var.autoscaler ? 1 : 0}"

  name   = "${var.service}-autoscaler-${var.gcp_region}"
  region = "${var.gcp_region}"
  target = "${google_compute_region_instance_group_manager.autoscaled_managed_group.self_link}"

  autoscaling_policy = {
    max_replicas    = "${var.asg_max}"
    min_replicas    = "${var.asg_min}"
    cooldown_period = 60

    cpu_utilization {
      target = "${var.cpu_utilization}"
    }
  }
}
